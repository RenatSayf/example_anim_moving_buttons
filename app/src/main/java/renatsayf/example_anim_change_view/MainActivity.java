package renatsayf.example_anim_change_view;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity
{
    ButtonsFragment buttonsFragment;
    FragmentTransaction transaction;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if (savedInstanceState == null)
        {
            buttonsFragment = new ButtonsFragment();
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.fragment_layout, buttonsFragment);
            transaction.commit();
        }

    }

}
