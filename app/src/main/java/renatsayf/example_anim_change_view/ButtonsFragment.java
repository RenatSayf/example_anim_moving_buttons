package renatsayf.example_anim_change_view;


import android.animation.Animator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class ButtonsFragment extends Fragment
{
    private LinearLayout linLayout;
    private ViewPropertyAnimator animToLeft, animToDown;

    private long duration = 1000;
    private int btnIndex = -1;
    private int height;

    private String KEY_TEXT_ARRAY = "text_array";

    public ButtonsFragment()
    {

    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        ArrayList<String> list = saveButtonsLayoutState(linLayout);
        outState.putStringArrayList(KEY_TEXT_ARRAY, list);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View fragment_view = inflater.inflate(R.layout.fragment_buttons, container, false);
        linLayout = (LinearLayout) fragment_view.findViewById(R.id.left_layout);

        if (savedInstanceState == null)
        {
            fillLayout(linLayout);
        }

        if (savedInstanceState != null)
        {
            ArrayList<String> list = savedInstanceState.getStringArrayList(KEY_TEXT_ARRAY);
            if (list != null && list.size() > 0)
            {
                for (int i = 0; i < list.size(); i++)
                {
                    Button button = (Button) linLayout.getChildAt(i);
                    button.setText(list.get(i));
                    button_OnClick(button, i);
                }
            }
        }
        return fragment_view;
    }

    private ArrayList<String> saveButtonsLayoutState(LinearLayout linearLayout)
    {
        ArrayList<String> textArray = new ArrayList<>();
        Map<Float, String> stringMap = new HashMap<>();
        for (int i = 0; i < linearLayout.getChildCount(); i++)
        {
            Button button = (Button) linearLayout.getChildAt(i);
            stringMap.put(button.getY(), button.getText().toString());
        }

        Map<Float, String> sortBtnsLayout = new TreeMap<>(new Comparator<Float>()
        {
            @Override
            public int compare(Float lhs, Float rhs)
            {
                return lhs.compareTo(rhs);
            }
        });
        sortBtnsLayout.putAll(stringMap);

        Collection<String> stringCollection = sortBtnsLayout.values();
        textArray.clear();
        for (String item : stringCollection)
        {
            textArray.add(item);
        }
        return textArray;
    }

    private void fillLayout(LinearLayout linearLayout)
    {
        Button button;
        for (int i = 0; i < linearLayout.getChildCount(); i++)
        {
            button = (Button) linLayout.getChildAt(i);
            button.setText("Кнопка "+i);
            button_OnClick(button, i);
        }
    }

    private void button_OnClick(final View view, final int index)
    {
        view.setOnClickListener(new View.OnClickListener()
        {
            int width;
            @Override
            public void onClick(final View button)
            {
                Toast.makeText(getActivity(),"Клик по кнопке = "+ index,Toast.LENGTH_SHORT).show();
                width = button.getWidth();
                height = button.getHeight();
                btnIndex = index;

                if (btnIndex >= 0)
                {
                    animToLeft = linLayout.getChildAt(btnIndex).animate().translationX(-(width + 20))
                            .setDuration(duration)
                            .setInterpolator(new AccelerateDecelerateInterpolator());

                    animToLeft_Listener(animToLeft);
                }
            }
        });
    }

    private void animToLeft_Listener(ViewPropertyAnimator animator)
    {
        if (animator == null)
        {
            return;
        }
        animator.setListener(new Animator.AnimatorListener()
        {
            @Override
            public void onAnimationStart(Animator animator)
            {

            }

            @Override
            public void onAnimationEnd(Animator animator)
            {
                if (btnIndex == 0)
                {
                    linLayout.getChildAt(0).animate().translationX(0);
                }

                if (btnIndex > 0)
                {
                    for (int i = btnIndex -1; i >= 0; i--)
                    {
                        animToDown = linLayout.getChildAt(i).animate().translationY(height).setDuration(duration).setInterpolator(new AccelerateDecelerateInterpolator());
                        if (i == 0)
                        {
                            animToDown_Listener(animToDown);
                        }
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animator)
            {

            }

            @Override
            public void onAnimationRepeat(Animator animator)
            {

            }
        });
    }

    private void animToDown_Listener(ViewPropertyAnimator animator)
    {
        animator.setListener(new Animator.AnimatorListener()
        {
            @Override
            public void onAnimationStart(Animator animator)
            {

            }

            @Override
            public void onAnimationEnd(Animator animator)
            {
                linLayout.getChildAt(btnIndex).setY(linLayout.getTop());
                linLayout.getChildAt(btnIndex).animate().translationX(0).setDuration(duration).setInterpolator(new AccelerateDecelerateInterpolator());
                animToLeft.setListener(null);
            }

            @Override
            public void onAnimationCancel(Animator animator)
            {

            }

            @Override
            public void onAnimationRepeat(Animator animator)
            {

            }
        });
    }


}
